import axios from '@/services/ajax';

class Form {

    sendForm(data) {
        return axios.post("form", data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .catch(error => {
                return Promise.reject(error);
            });
    }

    downloadLink(data) {
        return axios.get("link/" + data, { responseType: 'arraybuffer' })
            .catch(error => {
                return Promise.reject(error);
            });
    }

    getFileInfo(data) {
        return axios.get("file/" + data)
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

const FormRepository = new Form();
export default FormRepository;
