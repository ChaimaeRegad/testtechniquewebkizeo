import axios from '@/services/ajax';

class Auth {

    postSignUp(data) {
        return axios.post("register", data)
            .catch(error => {
                return Promise.reject(error);
            });
    }

    postSignIn(data) {
        return axios.post("login", data)
            .catch(error => {
                return Promise.reject(error);
            });
    }

    logout() {
        return axios.delete("login")
            .catch(error => {
                return Promise.reject(error);
            });
    }

    checklogin(data) {
        return axios.get("login", data)
            .catch(error => {
                return Promise.reject(error);
            });
    }

}

const AuthRepository = new Auth();
export default AuthRepository;
