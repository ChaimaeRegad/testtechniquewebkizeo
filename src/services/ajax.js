import axios from 'axios';
import config from '@/config';
import store from '@/store';
//import {EventBus, Events} from '@/services/eventbus';

const instance = axios.create({
  baseURL: config.APP_API_URL,
  timeout: 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }
});

let wstorage = window.localStorage;
let vuexString = wstorage.getItem('vuex');

if (vuexString) {
  let vueObj = JSON.parse(vuexString);
  if (vueObj.devicePrefs && vueObj.devicePrefs.bearerToken) {
    instance.defaults.headers.common['Authorization'] = 'Bearer ' + vueObj.devicePrefs.bearerToken;
  }
}

window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
  
  if (event.data.type === 'login') {
    // Configure token
   store.commit('devicePrefs/UPDATE_BEARER_TOKEN', event.data.token);
    instance.defaults.headers.common['Authorization'] = 'Bearer ' + event.data.token;
    store.commit('general/SET_iS_AUTHENTICATED', true );
   // EventBus.$emit(Events.APP_LOGIN, true);
  }

}


instance.interceptors.response.use(response => {
  return response;
}, error => {
  // If it is an HTTP error
  if(error.response) {
      if(error.response.status == 420
        || error.response.status == 419
        || error.response.status == 401) { // Session expired.
          // Logout user from the app
          let vuexString = window.localStorage.getItem('vuex');
          if (vuexString) {
            let vueObj = JSON.parse(vuexString);
            if (vueObj.devicePrefs && vueObj.devicePrefs.bearerToken) {
              window.localStorage.removeItem('vuex');
                window.location.href = "/";
            }
          }
      } else {
          // other errors
      }
  }

  return Promise.reject(error);
});

export default instance;
