import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import AuthRepository from './repositories/auth'
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
  created() {
    let token = window.localStorage.getItem('vuex')
    token = JSON.parse(token);
    token = token.devicePrefs.bearerToken;
    if (token) {
      console.log(token,'token')
      AuthRepository.checklogin()
      .then((res) => {
        console.log(res,'response')
        if(res.data.user){
          console.log('logged');
          store.commit('general/SET_iS_AUTHENTICATED', true);
        }else{
          window.localStorage.removeItem('vuex');
          store.commit('general/SET_iS_AUTHENTICATED', false);
          store.commit('devicePrefs/UPDATE_BEARER_TOKEN', null, {root: true});
        }
      
      })
      .catch(() => {
        //error display
      });
      
    } else {
      store.commit('general/SET_iS_AUTHENTICATED', false);
      console.log('no token', token)
    }
  }

}).$mount('#app')
