const state = {
    bearerToken: null,
  };
  
  const mutations = {
    UPDATE_BEARER_TOKEN(state, token) {
      return state.bearerToken = token;
    }
  };
  
  const actions = {
  
  };
  
  const getters = {

  };
  
  export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
  };
  