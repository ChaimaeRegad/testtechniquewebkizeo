
const state = {
    signInDialog: false,
    signUpDialog:false,
    isAuthenticated:false
};

const mutations = {
    SET_SIGNIN_DIALOG(state, value) {
        state.signInDialog = value
    },
    SET_SIGNUP_DIALOG(state, value) {
        state.signUpDialog = value
    },
    SET_iS_AUTHENTICATED(state, value) {
        state.isAuthenticated = value
    }

};

const actions = {

};

const getters = {

};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
