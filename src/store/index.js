import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import general from './modules/general'
import devicePrefs from './modules/devicePrefs'

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['devicePrefs'],
    })],
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        general: general,
        devicePrefs: devicePrefs
    }
})
